<?php
/**
 * @package shippingMethod
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: fedexfreightltl.php 4 2011-10-08 22:37:08Z numinix $
 */
 
define('MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TEXT_TITLE', 'FedEx Freight LTL');
define('MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TEXT_DESCRIPTION', 'FedEx Freight LTL');