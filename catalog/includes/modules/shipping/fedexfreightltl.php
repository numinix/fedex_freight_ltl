<?php
/**
 * @package shippingMethod
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: fedexfreightltl.php 4 2011-10-08 22:37:08Z numinix $
 */
 
class fedexfreightltl {

  var $code;
  var $title;
  var $description; 
  var $icon;  
  var $enabled;
  var $account;
  var $password;
  var $zip;
    
  function fedexfreightltl() {
    global $order, $db;
    
    $this->code = 'fedexfreightltl';
    $this->title = MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TEXT_TITLE;
    $this->description = MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TEXT_DESCRIPTION;
    $this->icon = '';
    $this->sort_order = MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SORT_ORDER;
    $this->error = false;
    
    if (zen_get_shipping_enabled($this->code)) {
      $this->enabled = ((MODULE_SHIPPING_FEDEX_FREIGHT_LTL_STATUS == 'true') ? true : false);
    }
    if ($this->enabled && (int)MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ZONE > 0) {
      $check_flag = false;
      $check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
      while (!$check->EOF) {
          if ($check->fields['zone_id'] < 1) {
              $check_flag = true;
              break;
          } elseif ($check->fields['zone_id'] == $order->delivery['zone_id']) {
              $check_flag = true;
              break;
          }
          $check->MoveNext();
      }

      if ($check_flag == false) {
          $this->enabled = false;
      }
    }   
  }  
  
  function quote($method = '') {
	
    global $order, $shipping_weight, $shipping_num_boxes, $db;
    $rates = array();
    if ($this->enabled && isset($order->delivery['postcode'])) {
      // build requests
 	if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",date('Y-m-d G:i:s')."\n");  
      if (MODULE_SHIPPING_FEDEX_FREIGHT_LTL_PRIORITY == 'true') {
        $priority_url = $this->buildRequest('Priority');
		if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Priority URL:'."\n".$priority_url."\n\n", FILE_APPEND);
        $priority = file_get_contents($priority_url);
		if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Priority Response:'."\n".$priority."\n\n", FILE_APPEND);
        $doc = new DOMDocument();
        $doc->loadXML($priority);
        $rate = $doc->getElementsByTagName('net-freight-charges');
        if ($rate && $rate->item(0)->nodeValue > 0) {
          $rates['Priority'] = $rate->item(0)->nodeValue;
        } 
      }
      if (MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ECONOMY == 'true') {
		  
        $economy_url = $this->buildRequest('Economy');
		if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Economy URL:'."\n".$economy_url."\n\n", FILE_APPEND);
        $economy = file_get_contents($economy_url);
		if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Economy Response:'."\n".$economy."\n\n", FILE_APPEND);
        $doc = new DOMDocument();
        $doc->loadXML($economy);
        $rate = $doc->getElementsByTagName('net-freight-charges');
        if ($rate && $rate->item(0)->nodeValue > 0) {
          $rates['Economy'] = $rate->item(0)->nodeValue;
        }
      }

      if (sizeof($rates) > 0) {
        switch (SHIPPING_BOX_WEIGHT_DISPLAY) {
          case (0):
          $show_box_weight = '';
          break;
          case (1):
          $show_box_weight = ' (' . $shipping_num_boxes . ' ' . TEXT_SHIPPING_BOXES . ')';
          break;
          case (2):
          $show_box_weight = ' (' . number_format($shipping_weight * $shipping_num_boxes,2) . TEXT_SHIPPING_WEIGHT . ')';
          break;
          default:
          $show_box_weight = ' (' . $shipping_num_boxes . ' x ' . number_format($shipping_weight,2) . TEXT_SHIPPING_WEIGHT . ')';
          break;
        }
        $this->quotes = array('id' => $this->code,
                              'module' => $this->title . $show_box_weight);
        $methods = array();
        foreach($rates as $method_name => $rate) {
          if ($method == '' || (str_replace('_', '', $method_name) == $method)) {
            $methods[] = array('id' => str_replace('_', '', $method_name),
                               'title' => ucwords(strtolower(str_replace('_', ' ', $method_name))),
                               'cost' => $rate + MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE + ($rate * (MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE_PERCENT / 100)));
          }
        }
        $this->quotes['methods'] = $methods;
        if ($this->tax_class > 0) {
          $this->quotes['tax'] = zen_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
        }
        if (zen_not_null($this->icon)) $this->quotes['icon'] = zen_image($this->icon, $this->title);
        return $this->quotes;
      }
    }
  }
  
  function buildRequest($method) {
    global $order, $shipping_weight, $shipping_num_boxes, $db;
    $base_url = 'http://www.fedexfreight.fedex.com/XMLLTLRatingNoCity.jsp?';   
    
    $regKey = MODULE_SHIPPING_FEDEX_FREIGHT_LTL_REGKEY;
    if (MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ACCOUNT != '') {
      $as_acctnbr = MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ACCOUNT;
    }
    $as_opco = $method;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO == 'true'){
        $as_iamthe = 'billto';
    }
    else{
       $as_iamthe = 'shipper';
    }
    //$as_iamthe = 'billto';
    $as_shipterms = 'prepaid';
    $addons_request_url = '';
    
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ADDRS != '') $addons_request_url .= '&as_billto_address='.MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ADDRS;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CITY != '') $addons_request_url .= '&as_billto_city='.MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CITY;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ZIP != '') $addons_request_url .= '&as_billto_zip='.MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ZIP;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_STATE != '') $addons_request_url .= '&as_billto_state='.MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_STATE;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CTRY != '') $addons_request_url .= '&as_billto_country='.MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CTRY;
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_LIFTGATE == 'true') $addons_request_url .= '&as_liftgate=Y';
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREEZABLE == 'true') $addons_request_url .= '&as_freezable=Y';
    if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_HAZMAT == 'true') $addons_request_url .= '&as_haz1=Y';
    
    $as_shzip = str_replace(array(' ', '-'), '', SHIPPING_ORIGIN_ZIP);
    $country_code = $db->Execute("SELECT countries_iso_code_2 FROM " . TABLE_COUNTRIES . " WHERE countries_id = " . (int)SHIPPING_ORIGIN_COUNTRY . " LIMIT 1;");
    $as_shcntry = $country_code->fields['countries_iso_code_2'];
    $as_cnzip = str_replace(array(' ', '-'), '', $order->delivery['postcode']);
    $as_cncntry = $order->delivery['country']['iso_code_2'];
    $weights = '';
    for ($i=1; $i<=$shipping_num_boxes;$i++) {
      
      $weights .= '&as_class' . $i . '=' . MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREIGHTCLASS . '&as_weight' . $i . '=' . ceil($shipping_weight);
    }
	if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Weights:'."\n".$weights."\n\n", FILE_APPEND);
    $request_url = $base_url . 'regKey=' . $regKey . '&as_opco=' . $as_opco . '&as_iamthe=' . $as_iamthe . '&as_shipterms=' . $as_shipterms .$addons_request_url .'&as_shzip=' . $as_shzip . '&as_shcntry=' . $as_shcntry . '&as_cnzip=' . $as_cnzip . '&as_cncntry=' . $as_cncntry . $weights . '&as_acctnbr=' . $as_acctnbr ;
        if(MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG == 'true')file_put_contents(dirname(__FILE__)."/debug_fedex.txt",'Req URL:'."\n".$request_url."\n\n", FILE_APPEND);
   return $request_url;
  }
  
  function check() {
      global $db;
      if (!isset($this->_check)) {
          $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_STATUS'");
          $this->_check = $check_query->RecordCount();
      }
      return $this->_check;
  }
  
  function install() {
    global $db;
  
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable FedEx LTL Shipping', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_STATUS', 'true', 'Do you want to offer Fedex Freight LTL shipping?', '6', '1', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your FedEx Account Number', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ACCOUNT', '', 'Enter the FedEx Freight Account Number assigned to you (optional)', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your FedEx Freight LTL Reg Key', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_REGKEY', '', 'Enter the FedEx Freight LTL Registration Key assigned to you this is a number just for FEDEX LTL, it is 10 numbers a pipe and 10 more numbers (required)<br/><b>Example:</b> 1234567890|0123456789', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Guarantee', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_GUARANTEE', 'false', 'Guaranteed delivery service for FedEx Freight Priority or FedEx Freight Economy', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Priority', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_PRIORITY', 'false', 'Return FedEx Freight Priority rate quotes?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Economy', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ECONOMY', 'false', 'Return FedEx Freight Economy rate quotes?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Freight Class', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREIGHTCLASS', '050', 'Select a Freight Class', '6', '3', 'zen_cfg_select_option(array(\'050\', \'055\', \'060\', \'065\', \'070\', \'077\', \'085\', \'092\', \'100\', \'110\', \'125\', \'150\', \'175\', \'200\',  \'250\', \'300\',  \'400\', \'500\'), ', now())");        
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Fixed surcharge', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE', '0', 'Fixed surcharge amount to add to shipping charge?', '6', '4', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Variable surcharge', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE_PERCENT', '0', 'Percentage surcharge amount to add to shipping charge?', '6', '4', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '5', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Shipping Zone', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '98', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('<b>3rd Party Bill To:</b>', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO', 'false', 'Are you Using 3rd Party Bill to? (Are you Dropshipping?)', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Address', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ADDRS', '', 'Address for Bill To', '6', '', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('City', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CITY', '', 'City for Bill To', '6', '', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('State', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_STATE', '', 'State for Bill To', '6', '', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zip', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ZIP', '', 'Zip for Bill To', '6', '', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Country', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CTRY', '', 'Country for Bill To', '6', '', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Liftgate', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_LIFTGATE', 'false', 'Calculate with Liftgate', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function,date_added) values ('Freezable', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREEZABLE', 'false', 'Calculate as Freezable', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Hazardous Material', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_HAZMAT', 'false', 'Calculate as Hazardous Materials', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Debug', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG', 'false', 'Turn On Debugging?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SORT_ORDER', '0', 'Sort order of display.', '6', '99', now())");
  }
  
  function remove() {
    global $db;
    
    $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
  }
  
  function keys() {
    return array('MODULE_SHIPPING_FEDEX_FREIGHT_LTL_STATUS',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ACCOUNT',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_REGKEY',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_GUARANTEE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_PRIORITY',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ECONOMY',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREIGHTCLASS',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SURCHARGE_PERCENT',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_TAX_CLASS',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_ZONE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_SORT_ORDER',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ADDRS',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CITY',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_ZIP',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_STATE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_BILL_TO_CTRY',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_LIFTGATE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_FREEZABLE',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_HAZMAT',
                  'MODULE_SHIPPING_FEDEX_FREIGHT_LTL_DEBUG'
                  ); 
  }
}